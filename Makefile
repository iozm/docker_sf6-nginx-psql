#!/bin/bash
#Credit to Codenip
#See https://www.youtube.com/watch?v=bqaMXiw1Xjw

DOCKER_BE = app-sf6-imss-be #Change for container defined whit be in docker-compose.yml
export APP_NETWORK = app-network #Change for network app

OS := $(shell uname)

ifeq ($(OS),Darwin)
	UID = $(shell id -u)
else ifeq ($(OS),Linux)
	UID = $(shell id -u)
else
	UID = 1000
endif

BASE_DIR = $(pwd)

help: ## Show this help message
	@echo 'usage: make [target]'
	@echo
	@echo 'targets:'
	@egrep '^(.+)\:\ ##\ (.+)' ${MAKEFILE_LIST} | column -t -c 2 -s ':#'

test:
	echo $target

build: ## Rebuilds all the containers
	#U_ID=${UID} docker-compose build
	U_ID=${UID} docker compose build

run: ## Start the containers
	docker network create ${APP_NETWORK} || true
	#U_ID=${UID} docker-compose up -d
	U_ID=${UID} docker compose up -d

stop: ## Stop the containers
	# U_ID=${UID} docker-compose stop
	U_ID=${UID} docker compose stop

ssh-be: ## ssh's into the be container
	U_ID=${UID} docker exec -it --user ${UID} ${DOCKER_BE} bash

create-db-test:
	docker exec --user ${UID} ${DOCKER_BE} php bin/console doctrine:database:drop --force --if-exists --env=test 
	docker exec --user ${UID} ${DOCKER_BE} php bin/console doctrine:database:create --env=test 
	docker exec --user ${UID} ${DOCKER_BE} php bin/console doctrine:migrations:migrate --env=test
	docker exec -it --user ${UID} ${DOCKER_BE} php bin/console hautelook:fixtures:load --env=test -n





