<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240509231830 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE articulo_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE categoria_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE detalle_ingreso_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE detalle_venta_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE folio_venta_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE grupo_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE ingreso_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE permiso_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE persona_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE stock_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE user_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE venta_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE articulo (id INT NOT NULL, categoria_id INT NOT NULL, codigo VARCHAR(13) DEFAULT NULL, nombre VARCHAR(100) NOT NULL, activo BOOLEAN NOT NULL, detalle TEXT NOT NULL, forma_venta SMALLINT NOT NULL, precio_venta VARCHAR(10) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_69E94E913397707A ON articulo (categoria_id)');
        $this->addSql('CREATE TABLE categoria (id INT NOT NULL, nombre VARCHAR(100) NOT NULL, descripcion VARCHAR(255) DEFAULT NULL, activo BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE detalle_ingreso (id INT NOT NULL, ingreso_id INT NOT NULL, articulo_id INT NOT NULL, cantidad VARCHAR(10) NOT NULL, precio_compra VARCHAR(10) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8445CAB7E70E8ADB ON detalle_ingreso (ingreso_id)');
        $this->addSql('CREATE INDEX IDX_8445CAB72DBC2FC9 ON detalle_ingreso (articulo_id)');
        $this->addSql('CREATE TABLE detalle_venta (id INT NOT NULL, venta_id INT DEFAULT NULL, articulo_id INT NOT NULL, fecha_created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, fecha_updated TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, cantidad VARCHAR(10) NOT NULL, descuento VARCHAR(10) DEFAULT NULL, precio_venta VARCHAR(10) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_5191A401F2A5805D ON detalle_venta (venta_id)');
        $this->addSql('CREATE INDEX IDX_5191A4012DBC2FC9 ON detalle_venta (articulo_id)');
        $this->addSql('CREATE TABLE folio_venta (id INT NOT NULL, anio VARCHAR(4) NOT NULL, folio VARCHAR(5) NOT NULL, activo BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE grupo (id INT NOT NULL, nombre VARCHAR(50) DEFAULT NULL, descripcion VARCHAR(255) DEFAULT NULL, permisos VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE ingreso (id INT NOT NULL, persona_id INT NOT NULL, tipo_comprobante SMALLINT NOT NULL, numero_comprobante VARCHAR(50) DEFAULT NULL, fecha_registro DATE NOT NULL, impuesto VARCHAR(5) NOT NULL, total_compra VARCHAR(15) NOT NULL, user_crated INT NOT NULL, user_updated INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_CC9B241FF5F88DB9 ON ingreso (persona_id)');
        $this->addSql('CREATE TABLE permiso (id INT NOT NULL, role VARCHAR(50) DEFAULT NULL, descripcion VARCHAR(255) DEFAULT NULL, activo BOOLEAN DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE persona (id INT NOT NULL, tipo_persona SMALLINT NOT NULL, rfc VARCHAR(13) DEFAULT NULL, nombre VARCHAR(150) NOT NULL, direccion VARCHAR(255) DEFAULT NULL, telefono VARCHAR(10) DEFAULT NULL, email VARCHAR(150) NOT NULL, codigo_postal VARCHAR(5) DEFAULT NULL, perfil_fiscal SMALLINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE stock (id INT NOT NULL, articulo_id INT NOT NULL, cantidad VARCHAR(255) NOT NULL, fecha_created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, fecha_updated TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4B3656602DBC2FC9 ON stock (articulo_id)');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, email VARCHAR(180) NOT NULL, password VARCHAR(255) NOT NULL, nombre VARCHAR(50) DEFAULT NULL, apellido_paterno VARCHAR(50) DEFAULT NULL, apellido_materno VARCHAR(50) DEFAULT NULL, activo BOOLEAN DEFAULT NULL, ultimo_acceso TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('CREATE TABLE user_permiso (user_id INT NOT NULL, permiso_id INT NOT NULL, PRIMARY KEY(user_id, permiso_id))');
        $this->addSql('CREATE INDEX IDX_A577B594A76ED395 ON user_permiso (user_id)');
        $this->addSql('CREATE INDEX IDX_A577B5946CEFAD37 ON user_permiso (permiso_id)');
        $this->addSql('CREATE TABLE user_grupo (user_id INT NOT NULL, grupo_id INT NOT NULL, PRIMARY KEY(user_id, grupo_id))');
        $this->addSql('CREATE INDEX IDX_6ECC608BA76ED395 ON user_grupo (user_id)');
        $this->addSql('CREATE INDEX IDX_6ECC608B9C833003 ON user_grupo (grupo_id)');
        $this->addSql('CREATE TABLE venta (id INT NOT NULL, persona_id INT DEFAULT NULL, user_id INT NOT NULL, fecha_created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, fecha_updated TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, fecha_venta DATE NOT NULL, folio VARCHAR(6) NOT NULL, tipo_pago SMALLINT NOT NULL, total VARCHAR(10) NOT NULL, estatus BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8FE7EE55F5F88DB9 ON venta (persona_id)');
        $this->addSql('CREATE INDEX IDX_8FE7EE55A76ED395 ON venta (user_id)');
        $this->addSql('CREATE TABLE messenger_messages (id BIGSERIAL NOT NULL, body TEXT NOT NULL, headers TEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, available_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, delivered_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_75EA56E0FB7336F0 ON messenger_messages (queue_name)');
        $this->addSql('CREATE INDEX IDX_75EA56E0E3BD61CE ON messenger_messages (available_at)');
        $this->addSql('CREATE INDEX IDX_75EA56E016BA31DB ON messenger_messages (delivered_at)');
        $this->addSql('CREATE OR REPLACE FUNCTION notify_messenger_messages() RETURNS TRIGGER AS $$
            BEGIN
                PERFORM pg_notify(\'messenger_messages\', NEW.queue_name::text);
                RETURN NEW;
            END;
        $$ LANGUAGE plpgsql;');
        $this->addSql('DROP TRIGGER IF EXISTS notify_trigger ON messenger_messages;');
        $this->addSql('CREATE TRIGGER notify_trigger AFTER INSERT OR UPDATE ON messenger_messages FOR EACH ROW EXECUTE PROCEDURE notify_messenger_messages();');
        $this->addSql('ALTER TABLE articulo ADD CONSTRAINT FK_69E94E913397707A FOREIGN KEY (categoria_id) REFERENCES categoria (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE detalle_ingreso ADD CONSTRAINT FK_8445CAB7E70E8ADB FOREIGN KEY (ingreso_id) REFERENCES ingreso (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE detalle_ingreso ADD CONSTRAINT FK_8445CAB72DBC2FC9 FOREIGN KEY (articulo_id) REFERENCES articulo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE detalle_venta ADD CONSTRAINT FK_5191A401F2A5805D FOREIGN KEY (venta_id) REFERENCES venta (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE detalle_venta ADD CONSTRAINT FK_5191A4012DBC2FC9 FOREIGN KEY (articulo_id) REFERENCES articulo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ingreso ADD CONSTRAINT FK_CC9B241FF5F88DB9 FOREIGN KEY (persona_id) REFERENCES persona (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE stock ADD CONSTRAINT FK_4B3656602DBC2FC9 FOREIGN KEY (articulo_id) REFERENCES articulo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_permiso ADD CONSTRAINT FK_A577B594A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_permiso ADD CONSTRAINT FK_A577B5946CEFAD37 FOREIGN KEY (permiso_id) REFERENCES permiso (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_grupo ADD CONSTRAINT FK_6ECC608BA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_grupo ADD CONSTRAINT FK_6ECC608B9C833003 FOREIGN KEY (grupo_id) REFERENCES grupo (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE venta ADD CONSTRAINT FK_8FE7EE55F5F88DB9 FOREIGN KEY (persona_id) REFERENCES persona (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE venta ADD CONSTRAINT FK_8FE7EE55A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE articulo_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE categoria_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE detalle_ingreso_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE detalle_venta_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE folio_venta_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE grupo_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE ingreso_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE permiso_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE persona_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE stock_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE user_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE venta_id_seq CASCADE');
        $this->addSql('ALTER TABLE articulo DROP CONSTRAINT FK_69E94E913397707A');
        $this->addSql('ALTER TABLE detalle_ingreso DROP CONSTRAINT FK_8445CAB7E70E8ADB');
        $this->addSql('ALTER TABLE detalle_ingreso DROP CONSTRAINT FK_8445CAB72DBC2FC9');
        $this->addSql('ALTER TABLE detalle_venta DROP CONSTRAINT FK_5191A401F2A5805D');
        $this->addSql('ALTER TABLE detalle_venta DROP CONSTRAINT FK_5191A4012DBC2FC9');
        $this->addSql('ALTER TABLE ingreso DROP CONSTRAINT FK_CC9B241FF5F88DB9');
        $this->addSql('ALTER TABLE stock DROP CONSTRAINT FK_4B3656602DBC2FC9');
        $this->addSql('ALTER TABLE user_permiso DROP CONSTRAINT FK_A577B594A76ED395');
        $this->addSql('ALTER TABLE user_permiso DROP CONSTRAINT FK_A577B5946CEFAD37');
        $this->addSql('ALTER TABLE user_grupo DROP CONSTRAINT FK_6ECC608BA76ED395');
        $this->addSql('ALTER TABLE user_grupo DROP CONSTRAINT FK_6ECC608B9C833003');
        $this->addSql('ALTER TABLE venta DROP CONSTRAINT FK_8FE7EE55F5F88DB9');
        $this->addSql('ALTER TABLE venta DROP CONSTRAINT FK_8FE7EE55A76ED395');
        $this->addSql('DROP TABLE articulo');
        $this->addSql('DROP TABLE categoria');
        $this->addSql('DROP TABLE detalle_ingreso');
        $this->addSql('DROP TABLE detalle_venta');
        $this->addSql('DROP TABLE folio_venta');
        $this->addSql('DROP TABLE grupo');
        $this->addSql('DROP TABLE ingreso');
        $this->addSql('DROP TABLE permiso');
        $this->addSql('DROP TABLE persona');
        $this->addSql('DROP TABLE stock');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('DROP TABLE user_permiso');
        $this->addSql('DROP TABLE user_grupo');
        $this->addSql('DROP TABLE venta');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
