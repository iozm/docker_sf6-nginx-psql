<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240510063345 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE articulo_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE categoria_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE detalle_ingreso_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE detalle_venta_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE folio_venta_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE ingreso_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE persona_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE stock_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE venta_id_seq CASCADE');
        $this->addSql('ALTER TABLE ingreso DROP CONSTRAINT fk_cc9b241ff5f88db9');
        $this->addSql('ALTER TABLE articulo DROP CONSTRAINT fk_69e94e913397707a');
        $this->addSql('ALTER TABLE stock DROP CONSTRAINT fk_4b3656602dbc2fc9');
        $this->addSql('ALTER TABLE venta DROP CONSTRAINT fk_8fe7ee55f5f88db9');
        $this->addSql('ALTER TABLE venta DROP CONSTRAINT fk_8fe7ee55a76ed395');
        $this->addSql('ALTER TABLE detalle_venta DROP CONSTRAINT fk_5191a401f2a5805d');
        $this->addSql('ALTER TABLE detalle_venta DROP CONSTRAINT fk_5191a4012dbc2fc9');
        $this->addSql('ALTER TABLE detalle_ingreso DROP CONSTRAINT fk_8445cab7e70e8adb');
        $this->addSql('ALTER TABLE detalle_ingreso DROP CONSTRAINT fk_8445cab72dbc2fc9');
        $this->addSql('DROP TABLE ingreso');
        $this->addSql('DROP TABLE articulo');
        $this->addSql('DROP TABLE persona');
        $this->addSql('DROP TABLE stock');
        $this->addSql('DROP TABLE venta');
        $this->addSql('DROP TABLE detalle_venta');
        $this->addSql('DROP TABLE folio_venta');
        $this->addSql('DROP TABLE detalle_ingreso');
        $this->addSql('DROP TABLE categoria');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SEQUENCE articulo_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE categoria_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE detalle_ingreso_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE detalle_venta_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE folio_venta_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE ingreso_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE persona_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE stock_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE venta_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE ingreso (id INT NOT NULL, persona_id INT NOT NULL, tipo_comprobante SMALLINT NOT NULL, numero_comprobante VARCHAR(50) DEFAULT NULL, fecha_registro DATE NOT NULL, impuesto VARCHAR(5) NOT NULL, total_compra VARCHAR(15) NOT NULL, user_crated INT NOT NULL, user_updated INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_cc9b241ff5f88db9 ON ingreso (persona_id)');
        $this->addSql('CREATE TABLE articulo (id INT NOT NULL, categoria_id INT NOT NULL, codigo VARCHAR(13) DEFAULT NULL, nombre VARCHAR(100) NOT NULL, activo BOOLEAN NOT NULL, detalle TEXT NOT NULL, forma_venta SMALLINT NOT NULL, precio_venta VARCHAR(10) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_69e94e913397707a ON articulo (categoria_id)');
        $this->addSql('CREATE TABLE persona (id INT NOT NULL, tipo_persona SMALLINT NOT NULL, rfc VARCHAR(13) DEFAULT NULL, nombre VARCHAR(150) NOT NULL, direccion VARCHAR(255) DEFAULT NULL, telefono VARCHAR(10) DEFAULT NULL, email VARCHAR(150) NOT NULL, codigo_postal VARCHAR(5) DEFAULT NULL, perfil_fiscal SMALLINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE stock (id INT NOT NULL, articulo_id INT NOT NULL, cantidad VARCHAR(255) NOT NULL, fecha_created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, fecha_updated TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX uniq_4b3656602dbc2fc9 ON stock (articulo_id)');
        $this->addSql('CREATE TABLE venta (id INT NOT NULL, persona_id INT DEFAULT NULL, user_id INT NOT NULL, fecha_created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, fecha_updated TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, fecha_venta DATE NOT NULL, folio VARCHAR(6) NOT NULL, tipo_pago SMALLINT NOT NULL, total VARCHAR(10) NOT NULL, estatus BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_8fe7ee55a76ed395 ON venta (user_id)');
        $this->addSql('CREATE INDEX idx_8fe7ee55f5f88db9 ON venta (persona_id)');
        $this->addSql('CREATE TABLE detalle_venta (id INT NOT NULL, venta_id INT DEFAULT NULL, articulo_id INT NOT NULL, fecha_created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, fecha_updated TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, cantidad VARCHAR(10) NOT NULL, descuento VARCHAR(10) DEFAULT NULL, precio_venta VARCHAR(10) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_5191a4012dbc2fc9 ON detalle_venta (articulo_id)');
        $this->addSql('CREATE INDEX idx_5191a401f2a5805d ON detalle_venta (venta_id)');
        $this->addSql('CREATE TABLE folio_venta (id INT NOT NULL, anio VARCHAR(4) NOT NULL, folio VARCHAR(5) NOT NULL, activo BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE detalle_ingreso (id INT NOT NULL, ingreso_id INT NOT NULL, articulo_id INT NOT NULL, cantidad VARCHAR(10) NOT NULL, precio_compra VARCHAR(10) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_8445cab72dbc2fc9 ON detalle_ingreso (articulo_id)');
        $this->addSql('CREATE INDEX idx_8445cab7e70e8adb ON detalle_ingreso (ingreso_id)');
        $this->addSql('CREATE TABLE categoria (id INT NOT NULL, nombre VARCHAR(100) NOT NULL, descripcion VARCHAR(255) DEFAULT NULL, activo BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE ingreso ADD CONSTRAINT fk_cc9b241ff5f88db9 FOREIGN KEY (persona_id) REFERENCES persona (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE articulo ADD CONSTRAINT fk_69e94e913397707a FOREIGN KEY (categoria_id) REFERENCES categoria (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE stock ADD CONSTRAINT fk_4b3656602dbc2fc9 FOREIGN KEY (articulo_id) REFERENCES articulo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE venta ADD CONSTRAINT fk_8fe7ee55f5f88db9 FOREIGN KEY (persona_id) REFERENCES persona (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE venta ADD CONSTRAINT fk_8fe7ee55a76ed395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE detalle_venta ADD CONSTRAINT fk_5191a401f2a5805d FOREIGN KEY (venta_id) REFERENCES venta (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE detalle_venta ADD CONSTRAINT fk_5191a4012dbc2fc9 FOREIGN KEY (articulo_id) REFERENCES articulo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE detalle_ingreso ADD CONSTRAINT fk_8445cab7e70e8adb FOREIGN KEY (ingreso_id) REFERENCES ingreso (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE detalle_ingreso ADD CONSTRAINT fk_8445cab72dbc2fc9 FOREIGN KEY (articulo_id) REFERENCES articulo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
