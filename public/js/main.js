function loadOptionsSelect(url, data, select) {
   $.ajax({
        type: 'GET',
        url: url,
        data: data,     
        success: function(data){
            $("#"+select).html("");
            $("#"+select).append('<option value="">Seleccione...</option>');
            $.each(data, function(i,item){
                $("#"+select).append('<option value="'+item.id+'">'+item.name+'</option>');
                $("#"+select).change();
            });                        
        }
    });
}

function formToMayusText(formNames)
{
    $("form[name='" + formNames +"']  textarea").keyup(function(){
        val = $(this).val();
        val = omitirAcentos(val);
        $(this).val(val.toUpperCase());
    });

    return;
}

function formToMayus(formName)
{
    $("form[name='" + formName +"']  input").keyup(function(){
        val = $(this).val();
        val = omitirAcentos(val);
        $(this).val(val.toUpperCase());
    });
    
    return;
}

function omitirAcentos(text) {
    var acentos = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÇç";
    var original = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuucc";
    for (var i=0; i<acentos.length; i++) {
        text = text.replace(new RegExp(acentos.charAt(i), 'g'), original.charAt(i));
    }
    return text;
}

function createNotification(type, message)
{
    toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": 300,
                "hideDuration": 100,
                "timeOut": 5000,
                "extendedTimeOut": 1000,
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
              }
              toastr[type](message);
}

function createAlertSwal(text)
{
    alertSwal = Swal.fire(
    {
        icon: "info",
        title: "Estas seguro?",
        text: text,
        type: "warning",
        showCancelButton: true,
        confirmButtonText:"<span class='fas fa-thumbs-up'></span> Confirmar",
        confirmButtonClass:"btn btn-lg btn-success",
        cancelButtonClass:"btn btn-danger",
        cancelButtonText:"<span class='fas fa-thumbs-down'></span> Cancelar"
    });
    
    return alertSwal;
}

function blockPage(options)
{
    options = $.extend(true, {
        opacity: 0.3,
        overlayColor: '#000000',
        state: 'brand',
        type: 'loader',
        size: 'lg',
        centerX: true,
        centerY: true,
        message: 'Procesando la Petición...',
        shadow: true,
        width: 'auto',
        baseZ: 2060
    }, options);
    
    var classes = 'm-blockui ' + (options.shadow === false ? 'm-blockui-no-shadow' : '');
    
    skin = options.skin ? 'm-loader--skin-' + options.skin : '';
    state = options.state ? 'm-loader--' + options.state : '';
    size = options.size ? 'm-loader--' + options.size : '';
    
    loading = '<div class="m-loader ' + skin + ' ' + state + ' ' + size + '"></div';
    html = '<div class="' + classes + '"><span>' + options.message + '</span><span>' + loading + '</span></div>';
    
    $.blockUI();
    
    var params = {
        message: html,
        centerY: options.centerY,
        centerX: options.centerX,
        baseZ:  options.baseZ,
        css: {
            top: '30%',
            left: '50%',
            border: '0',
            padding: '0',
            backgroundColor: 'none',
            width: options.width
            
        },
        overlayCSS: {
            backgroundColor: options.overlayColor,
            opacity: options.opacity,
            cursor: 'wait'
        }
    };
    
    params.css.top = '50%';
    $.blockUI(params);
}

function unblockPage()
{
    $.unblockUI();
}

function changeCustomFileInput(customFileInput)
{
    /* Add file name path to input files */
    $(customFileInput).change(function (e) {
        var files = [];
        for (var i = 0; i < $(this)[0].files.length; i++) {
                files.push($(this)[0].files[i].name);
        }
        $(this).next('.custom-file-label').html(files.join(', '));
    });
}

function initDatePicker(element, options = {})
{
    settings = {
        clearBtn: true,
        todayHighlight: true,
        orientation: "bottom left"
    };
    
    settings = $.extend({}, settings, options);
    
    $(element).datepicker(settings);
}

/*
Carga el calendario datepicker en español

$(document).ready(function(){
    $.fn.datepicker.defaults.language = 'es';
});
*/
