var responsiveHelper_dt_basic = undefined;
var responsiveHelper_datatable_fixed_column = undefined;
var responsiveHelper_datatable_col_reorder = undefined;
var responsiveHelper_datatable_tabletools = undefined;

var breakpointDefinition = {
        tablet : 1024,
        phone : 480
};


function getDataRowDT(dataTable, element) {
    if(dataTable == null) {
        alert("Indica el dataTable");
        return;
    }
    
    if(element == null ) {
        alert("Indica el elemento");
        return;
    }
    
    row = element.parents("tr").eq(0);
    index = dataTable.row(row).index();
    data = dataTable.row(row).data();
    
    return {"index": index, "data": data };
}

optionsDTLanguaje = {
            "lengthMenu": " _MENU_ ",
            "zeroRecords": "! No se encontraron registros",
            "info": "Se muestran _START_ a _END_ de _TOTAL_ registros",
            "infoEmpty": "No se encontraron registros",
            "infoFiltered": "(filtrado de _MAX_ registro(s) en total)",
            "searchPlaceholder": "Buscar",
        }


function getOptionsResponsive(table) {
    options = {

        "preDrawCallback" : function() {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#'+table), breakpointDefinition);
                }
        },
        "rowCallback" : function(nRow) {
                responsiveHelper_dt_basic.createExpandIcon(nRow);
        },
        "drawCallback" : function(oSettings) {
                responsiveHelper_dt_basic.respond();
        }
    }
    
    return options;
}


optionsDTComplete = {
    "autoWidth" : true,
    "bJQueryUI": false,
    "bSort": true,
    "bBootstrap": true,
    "aaSorting": [],
    "bInfo": true,
    "bPaginate": true,
    //"sPaginationType": "full_numbers",
    "language": optionsDTLanguaje,
    "dom": "<'row'<'col-sm-12 col-md-6 col-lg-6 d-sm-none d-md-block'f><'col-sm-12 col-md-6 col-lg-6'<'float-right pl-1' l><'float-right' B>>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            "buttons": [
                { extend: 'copy', className: 'btn btn-default', text: '<i class="fas fa-copy"></i> Copiar' },
                { extend: 'csv', className: 'btn btn-default', bom: true, text: '<i class="fas fa-file-csv"></i> CSV' }
            ]
};

optionsDTMinimal = {
    "bJQueryUI": false,
    "bSort": false,
    "aaSorting": [],
    "bInfo": false,
    "bPaginate": false,
    "bFilter": true,
    //"sPaginationType": "full_numbers",
    "oLanguage": optionsDTLanguaje,
    //"sDom": "<'row'<'col-sm-6 col-xs-5'l><'col-sm-6 col-xs-7'f>r>t<'row'<'col-sm-5 hidden-xs'i><'col-sm-7 col-xs-12 clearfix'p>>"
};

optionsDTMedium = {
    "bJQueryUI": false,
    "bSort": true,
    "aaSorting": [],
    "bInfo": true,
    "bPaginate": false,
    "bFilter": true,
    //"sPaginationType": "full_numbers",
    "oLanguage": optionsDTLanguaje,
    //"sDom": "<'row'<'col-sm-6 col-xs-5'l><'col-sm-6 col-xs-7'f>r>t<'row'<'col-sm-5 hidden-xs'i><'col-sm-7 col-xs-12 clearfix'p>>"
};

function createDataTable(type, table, options)
{
    if(type == null) {
        alert("Indica el tipo DataTable complete,medium o minimal.");
        return;
    }
    
    if(table == null) {
        alert("Indica el id de la tabla.");
        return;
    }
    //optionsResponsive = getOptionsResponsive(table);
    
    switch(type) {
        case "complete": 
            if(options == null) {
                settings = optionsDTComplete;
            } else {
                settings = $.extend({}, optionsDTComplete, options);
            }
            //dataTable =  $("#"+table).dataTable(settings);
            break;
        case "medium":
            if(options == null) {
                settings = optionsDTMedium;
            } else {
                settings = $.extend({}, optionsDTMedium, options);
            }
            
            break;
        case "minimal":
            
            if(options == null) {
                settings = optionsDTMinimal;
            } else {
                settings = $.extend({}, optionsDTMinimal, options);
            }
            
            break;
    }
    settings = $.extend({}, settings)
    
    dataTable =  $("#"+table).DataTable(settings);
    
    return dataTable;
}
/*
jQuery.fn.dataTableExt.oApi.fnSetFilteringDelay = function ( oSettings, iDelay ) {
    var _that = this;
 
    if ( iDelay === undefined ) {
        iDelay = 250;
    }
 
    this.each( function ( i ) {
        $.fn.dataTableExt.iApiIndex = i;
        var
            $this = this,
            oTimerId = null,
            sPreviousSearch = null,
            anControl = $( 'input', _that.fnSettings().aanFeatures.f );
 
            anControl.unbind( 'keyup search input' ).bind( 'keyup search input', function() {
            var $$this = $this;
 
            if (sPreviousSearch === null || sPreviousSearch != anControl.val()) {
                window.clearTimeout(oTimerId);
                sPreviousSearch = anControl.val();
                oTimerId = window.setTimeout(function() {
                    $.fn.dataTableExt.iApiIndex = i;
                    _that.fnFilter( anControl.val() );
                }, iDelay);
            }
        });
 
        return this;
    } );
    return this;
};
*/
