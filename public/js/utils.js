toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": true,
    "progressBar": true,
    "positionClass": "toast-top-right",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": 300,
    "hideDuration": 100,
    "timeOut": 5000,
    "extendedTimeOut": 1000,
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

$.validator.setDefaults( {
    errorElement: "em",
    errorPlacement: function ( error, element ) {
        // Add the `invalid-feedback` class to the error element
        error.addClass( "invalid-feedback" );

        if ( element.prop( "type" ) === "checkbox" ) {
            error.insertAfter( element.next( "label" ) );
        } else {
            error.insertAfter( element );
        }
    },
    highlight: function ( element, errorClass, validClass ) {
        $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
    },
    unhighlight: function (element, errorClass, validClass) {
        $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
    }
} );

$.validator.addMethod("autocomplete", function(value,  element, params) {    
    
    if($(params.dependence).val() == "") {
        return false;
    }
    return true;
}, "Seleccione un elmento de la lista.");

const helpers = {
    createNotification: (type, message, title = null, options = []) => {
        toastr[type](message, title);
    },
    createDataTable: (type, table, options = {}) => {
        const dataTable =  $("#"+table).DataTable({


        });

        return dataTable;
    },

    showFormErrors: (formErrors) => {
        formErrors.fields.forEach(element => {
            idErrorElement = element.field+"-error";

            $("#" + element.field).length;
            if($("#"+idErrorElement).length) {
                $("#"+idErrorElement).html(element.message);
                $("#"+idErrorElement).show();

            } else {
                errorElement = '<span id="'+idErrorElement+'" class="error invalid-feedback">'+element.message+'</span>';
                $(errorElement).insertAfter("#" + element.field);
            }

            $("#" + element.field).addClass('is-invalid');
        });
    },
    createConfirmation: (description = "Seguro de realizar la acción?") => {
        let swalWithBootstrapButtons = Swal.mixin(
            {
                customClass:
                {
                    confirmButton: "btn btn-primary mr-2",
                    cancelButton: "btn btn-danger "
                },
                buttonsStyling: false,
                icon: "info",
                title: "Confirmar",
                text: description,

                showCancelButton: true,
                confirmButtonText: '<i class="fal fa-check-circle"></i> Confirmar',
                cancelButtonText: '<i class="fal fa-times-circle"></i> Cancelar',

            });


        return swalWithBootstrapButtons;
    },
    blockUI: (message = 'Espere, procesando...') => {
        $.blockUI({
            message: '<span class="text-muted">'+message+'</span>',
            overlayCSS: { backgroundColor: '#000', opacity: 0.8 },
            css: {
                border: 'none',
                padding: '5px',
                backgroundColor: '#fff',

                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .9,
                color: '#fff'
            },
            baseZ: 2065,
            blockMsgClass: 'blockMsg',
        });
    },
    unblockUI: () => {

        $.unblockUI();
    }
}
