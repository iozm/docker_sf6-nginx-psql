let form = $("form[name=e_firma]");

form.validate();

const btnValidarFirma = document.querySelector('#btnValidarFirma');

const parseString = (s) => {
    s=s+"/";
    let parseArray = [];
    let clave = "";
    let val = "";
    let x = 0;
    for (let i = 0; i < s.length; i++) {
        if(s.charAt(i)==="/"){
            if(i!==0) parseArray[clave] = val;
                clave = "";
                val = "";
                x = 0;
        }else if(s.charAt(i)==="="){
            x = 1;
        }
        if(x===0 && s.charAt(i)!="/"){
            clave = clave + s.charAt(i);
        }else if(x===1 && s.charAt(i)!="="){
            val = val + s.charAt(i);
        }
    }
    return parseArray;
};

const findValueObjectCER = (object, type) => {
    let value = object.array.find( item  => item[0].type === type);
    return value[0].value;
};

async function readFileAsArrayBuffer(file) 
{
    let result_base64 = await new Promise((resolve) => {
        let fileReader = new FileReader();
        fileReader.onload = (e) => resolve(fileReader.result);
        fileReader.readAsArrayBuffer(file);
    });

    //console.log(result_base64); // aGV5IHRoZXJl...

    return result_base64;
}

const hex_to_ascii = (str1) =>  {
        var hex  = str1.toString();
        var str = '';
        for (var n = 0; n < hex.length; n += 2) {
            str += String.fromCharCode(parseInt(hex.substr(n, 2), 16));
        }
        return str;
    }

const getLocalDate = (zulu_time) => {
    let year = "20" + zulu_time.substring(0,2);
    let month = zulu_time.substring(2,4);
    let day = zulu_time.substring(4,6);
    let hour = zulu_time.substring(6,8);
    let min = zulu_time.substring(8,10);
    let sec = zulu_time.substring(10,12);
    return new Date(year+"-"+month+"-"+day+"T"+hour+":"+min+":"+sec+"Z");
};

const hexToBase64 =  (hexstring) => {
    
        return btoa(hexstring.match(/\w{2}/g).map(function(a) {
            return String.fromCharCode(parseInt(a, 16));
        }).join(""));
};


const getDataCertificate =  (contentFileCertificate) => {
    let data = {
        hSerial: "",
        sIssuer: "",
        sNotBefore: "",
        sNotAfter: "",
        sSubject: "",
        modulusCer: ""        
    };
    
    let bytes = new Uint8Array(contentFileCertificate);
    let binary = '';
    for (let i = 0; i < bytes.byteLength; i++) {
        binary += String.fromCharCode(bytes[i]);
    }
    let hex = rstrtohex(binary);
    
    let pemString = KJUR.asn1.ASN1Util.getPEMStringFromHex(hex, 'CERTIFICATE');    
    let certificado = new X509();
    certificado.readCertPEM(pemString);
    data.hSerial    = certificado.getSerialNumberHex();
    data.hSerialStr = hextorstr(certificado.getSerialNumberHex());
    data.sIssuer    = findValueObjectCER(certificado.getIssuer(), "O");
    let startDate = new Date(zulutodate(certificado.getNotBefore()).toUTCString());    
    data.sNotBefore = startDate;
    
    let endDate = new Date(zulutodate(certificado.getNotAfter()).toUTCString());    
    console.log(zulutodate(certificado.getNotAfter()));
    data.sNotAfter  = endDate;
    data.sSubject   = certificado.getSubject();    
    data.modulusCer = KEYUTIL.getKey(pemString).n.toString(16);
    
    return data;
    //console.log(data);
    
};

const validatePrivateKey = (contentFilePrivateKey, password, dataCertificate) => {
    let result = {
        rsakey: "",
        code: "",
        message: "",
    };
    let modulusKEY;
    
    let bytes = new Uint8Array(contentFilePrivateKey);
    let binary = '';
    for (let i = 0; i < bytes.byteLength; i++) {
        binary += String.fromCharCode(bytes[i]);
    }
    let keyhex = rstrtohex(binary);
    let pemString = KJUR.asn1.ASN1Util.getPEMStringFromHex(keyhex, 'ENCRYPTED PRIVATE KEY');
    try {     
        let rsakey = KEYUTIL.getKey(pemString, password, 'PKCS8PRV');
        result.rsakey = rsakey;
        modulusKEY = rsakey.n.toString(16);
    }catch (e) {
        result.code = 500;
        result.message = "La contraseña es incorrecta.";
        
        return result;
        
    }
    if (dataCertificate.modulusCer!== modulusKEY) {

        result.code = 500;
        result.message = "El certificado no corresponde con la llave privada.";
        return result;
        
    }else{    
        
        result.code = 200;
        return result;
    }
    
    return result;
};

const generateFirm = (dataValidateKey, value) => {     

    let sig = new KJUR.crypto.Signature({"alg": "SHA256withRSA"});
    sig.init(dataValidateKey.rsakey);
    sig.updateString(value);
    let sigValueHex = sig.sign();
    let firm = hexToBase64(sigValueHex);
    return firm;
    
};


btnValidarFirma.addEventListener("click", (e) => {
    e.preventDefault();
    
    if(form.valid()) {
        blockPage();
        let inputNombre = document.querySelector("#e_firma_nombre");
        let inputRFC = document.querySelector("#e_firma_rfc");
        let inputCURP = document.querySelector("#e_firma_curp");
        let inputFirma = document.querySelector("#e_firma_firma");
        let inputCadenaOriginal = document.querySelector("#e_firma_cadenaOriginal");
        
        let objFileCertificate = document.getElementById('e_firma_cer');
        var objFilePrivateKey = document.getElementById('e_firma_key');
        var password = document.getElementById('e_firma_password').value;
        var dataCertificate;
        var dataValidateKey;
        //validaFiel(objFileCertificate.files[0], objFilePrivateKey.files[0], password);
        //readFileAsArrayBuffer(objFileCertificate.files[0]).then((j)=> console.log(j));
        var contentOriginalCertificate;
        var contentOriginalPrivateKey;
        
        let readerCertificate = new FileReader();
        readerCertificate.readAsArrayBuffer (objFileCertificate.files[0]);
    
        readerCertificate.addEventListener('load', (e) => {
            
            contentOriginalCertificate = e.target.result;
            //console.log('Termina Carga Certificado');
            let readerPrivateKey = new FileReader();        
            readerPrivateKey.readAsArrayBuffer(objFilePrivateKey.files[0]);
            
            readerPrivateKey.addEventListener('load', (e2) => {
                
                //console.log('termina carga key');
                contentOriginalPrivateKey = e2.target.result;
                dataCertificate = getDataCertificate(contentOriginalCertificate);
                dataValidateKey = validatePrivateKey(contentOriginalPrivateKey, password, dataCertificate);
                if(dataValidateKey.code === 200) {
                    inputNombre.value = findValueObjectCER(dataCertificate.sSubject, "CN");
                    inputCURP.value = findValueObjectCER(dataCertificate.sSubject, "serialNumber");
                    inputRFC.value =  findValueObjectCER(dataCertificate.sSubject, "uniqueIdentifier");
                    var firm = generateFirm(dataValidateKey, inputCadenaOriginal.value);
                    
                    inputFirma.value =  firm;
                    //console.log(firm);
                    $("#btn-firmar-continuar").attr('disabled', false);
                    $("#btn-firmar-continuar").show();
                    unblockPage();
                    createNotification('success', 'La información se validó correctamente.');
                    
                    
                } else {
                    unblockPage();
                    createNotification('error', dataValidateKey.message);
                    return;
                }
                //Actualizar Inputs
                
            });
        });
    } else {
        createNotification('error', 'El formulario contiene errores.');
    }
});