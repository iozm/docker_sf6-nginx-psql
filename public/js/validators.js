jQuery.validator.setDefaults({
    highlight: function (element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-danger');
    },
    unhighlight: function (element) {
        $(element).closest('.form-group').removeClass('has-danger');
    },
   
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    }
});

//sobreescibiendo los mensajes del validador
$.extend(jQuery.validator.messages, {
    required: "Este campo es obligatorio",
    remote: "Please fix this field.",
    email: "Por favor escriba un email válido.",
    url: "Please enter a valid URL.",
    date: "Ingrese una fecha válida.",
    dateISO: "Please enter a valid date (ISO).",
    dateITA: "Ingrese una fecha válida.",
    number: "Igrese un número.",
    digits: "Please enter only digits.",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Ingresa el mismo valor.",
    accept: $.validator.format("Ingrese un archivo con un extension valida '{0}'."),
    maxlength: $.validator.format("Escriba mas de {0} caracteres."),
    minlength: $.validator.format("Escriba al menos {0} caracteres."),
    rangelength: $.validator.format("Please enter a value between {0} and {1} characters long."),
    range: $.validator.format("Please enter a value between {0} and {1}."),
    max: $.validator.format("Ingresa un valor menor que o igual a {0}."),
    min: $.validator.format("Ingresa un valor mayor que o igual a {0}."),
    pattern: "Proporciona un valor válido."

});

$.validator.addMethod("autocomplete", function(value,  element, params) {
    if($(params['dependence']).prop("value") == "") {
        return false;
    }
    return true;
}, "Selecciona una opción de la lista de resultados que se le muestra al escribir.");

jQuery.validator.addMethod("matricula", function(value, element) {
	//return this.optional(element) || /^90[2-5]\d\{2\}-\d{4}$/.test(value);
        return this.optional(element) || /^[0-9]{8}$/.test(value);
}, "Ingresa una matrícula válida");

jQuery.validator.addMethod("ceneval", function(value, element) {
	//return this.optional(element) || /^90[2-5]\d\{2\}-\d{4}$/.test(value);
        return this.optional(element) || /^[0-9]{9}$/.test(value);
}, "Ingrese un folio ceneval válido");

jQuery.validator.addMethod("curp", function(value, element) {
        return this.optional(element) || /^[A-Z]{1}[XAEIOU]{1}[A-Z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[B-DF-HJ-NP-TV-Z]{3}[0-9A-Z]{1}[0-9]{1}$/.test(value);
}, "Ingresa una CURP válida");

jQuery.validator.addMethod('filesize', function (value, element, param) {
    return this.optional(element) || (element.files[0].size <= param);
}, 'El archivo no debe pesar mas de 4 Megabytes');