# docker_sf6-nginx-psql

## Contenedores
Para iniciar la configuración de los contenedores copiar los siguientes archivos.

* `docker-compose.dist.yml` a `docker-compose.yml`. 
* `Makefile.dist` a `Makefile`.

### docker-compose.yml
Configurar el nombre de los contenedores y sus respectivas configuraciones.

### Makefile
Configurar el nombre del contenedor que tiene PHP `DOCKER_BE`.

Revisar los comandos que se ejecutan con Docker Compose, para la versión antigua es `docker-compose`y para la nueva versión es `docker compose`.

### Iniciar los contenedores
Iniciar los contenedores con `make run`.

## Instalar Dependencias
Ingresar al contenedor con el comando `make ssh-be`.

### Composer 
Instalar los vendor con el comando `composer install`. 

## Variables de entorno
Se tiene que crear el archivo `.env.local` para guardar las variables de entorno, en el archivo `.env` solo se podrán agregar variables comentadas, que servirán de referencia para agregarlas al `.env.local`.

Agrega la siguiente linea en la Entity `User` si utilizas Postgres `#[ORM\Table(name: '`user`')]`.

## Base de datos
Crear la base de datos `doctrine:database:create` y ejecuta las migraciones con el siguiente comando `doctrine:migrations:migrate`. 

### Cargando fixtures
Una vez creadas las tablas carga los datos de prueba con el comando `doctrine:fixtures:load --group=PermisoFixture`.

Importante, verifica en la carpeta si existen los archivos: 

* App\DataFixtures\UsuarioFixture
* App\DataFixtures\PermisoFixture