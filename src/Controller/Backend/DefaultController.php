<?php

namespace App\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class DefaultController extends AbstractController
{
    #[Route('/admin', name: 'app_default')]
    public function index(Request $request): Response
    {
       
        return $this->render('backend/default/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
        
    }

    
}

