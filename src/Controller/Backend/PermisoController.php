<?php

namespace App\Controller\Backend;

use App\Entity\Permiso;
use App\Form\PermisoType;
use App\Repository\PermisoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/permiso')]
class PermisoController extends AbstractController
{
    #[Route('/', name: 'app_permiso_index', methods: ['GET'])]
    public function index(PermisoRepository $permisoRepository): Response
    {
        return $this->render('backend/permiso/index.html.twig', [
            'permisos' => $permisoRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_permiso_new', methods: ['GET', 'POST'])]
    public function new(Request $request, PermisoRepository $permisoRepository): Response
    {
        $permiso = new Permiso();
        $form = $this->createForm(PermisoType::class, $permiso);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $permisoRepository->save($permiso, true);

            return $this->redirectToRoute('app_permiso_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('backend/permiso/new.html.twig', [
            'permiso' => $permiso,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_permiso_show', methods: ['GET'])]
    public function show(Permiso $permiso): Response
    {
        return $this->render('backend/permiso/show.html.twig', [
            'permiso' => $permiso,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_permiso_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Permiso $permiso, PermisoRepository $permisoRepository): Response
    {
        $form = $this->createForm(PermisoType::class, $permiso);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $permisoRepository->save($permiso, true);

            return $this->redirectToRoute('app_permiso_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('backend/permiso/edit.html.twig', [
            'permiso' => $permiso,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_permiso_delete', methods: ['POST'])]
    public function delete(Request $request, Permiso $permiso, PermisoRepository $permisoRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$permiso->getId(), $request->request->get('_token'))) {
            $permisoRepository->remove($permiso, true);
        }

        return $this->redirectToRoute('app_permiso_index', [], Response::HTTP_SEE_OTHER);
    }
}
