<?php

namespace App\Controller\Backend;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Doctrine\Persistence\ManagerRegistry;

#[Route('/admin/user')]
class UserController extends AbstractController
{
    #[Route('/', name: 'app_user_index', methods: ['GET'])]
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('backend/user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_user_new', methods: ['GET', 'POST'])]
    public function new(Request $request, UserPasswordHasherInterface $passwordHasher, ManagerRegistry $doctrine): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
        //    $userRepository->save($user, true);
            $entityManager = $doctrine->getManager();
            $plaintextPassword = $user->getPassword();
            $hashedPassword = $passwordHasher->hashPassword(
                $user,
                $plaintextPassword
            );
            $user->setPassword($hashedPassword);
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('backend/user/new.html.twig', [
            'user' => $user,
            'form' => $form,
        ]);
    }

    #[Route('/newUser', name: 'app_user_new_modal', methods: ['GET', 'POST'])]
    public function newModal(Request $request, UserPasswordHasherInterface $passwordHasher, ManagerRegistry $doctrine): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user, array(
            'action' => $this->generateUrl('app_user_new_modal'),
        ));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $doctrine->getManager();
            $plaintextPassword = $user->getPassword();
            $hashedPassword = $passwordHasher->hashPassword(
                $user,
                $plaintextPassword
            );
            $user->setPassword($hashedPassword);
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('success', 'User created successfully.');
            return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
                } elseif ($form->isSubmitted() && !$form->isValid()) {
                    $this->addFlash('error', 'User not created.');
                    return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
                }

        $html = $this->renderView('backend/user/new_modal.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);

        $datos = array(
            'code' => 200,
            'message' => '',
            'html' => $html,
        );

        return new JsonResponse($datos);


    }

    #[Route('/{id}', name: 'app_user_show', methods: ['GET'])]
    public function show(User $user): Response
    {
       /* return $this->render('backend/user/show.html.twig', [
            'user' => $user,
        ]); */

        $html = $this->renderView('backend/user/show.html.twig', [
            'user' => $user,
        ]);

        $datos = array(
            'code' => 200,
            'message' => '',
            'html' => $html,
        );
        return new JsonResponse($datos);
    }

    #[Route('/{id}/edit', name: 'app_user_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, User $user, UserRepository $userRepository): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userRepository->save($user, true);

            return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('backend/user/edit.html.twig', [
            'user' => $user,
            'form' => $form,
        ]);
    }

    #[Route('/{id}/editModal', name: 'app_user_edit_modal', methods: ['GET', 'POST'])]
    public function editModal(Request $request, User $user, UserPasswordHasherInterface $passwordHasher, 
                                ManagerRegistry $doctrine, UserRepository $userRepository): Response
    {
        $passwordOld = $user->getPassword();
        $form = $this->createForm(UserType::class, $user, array(
            'password_required' => false,
            'action' => $this->generateUrl('app_user_edit_modal', array(
                'id' => $user->getId(),
            )),
            
        ));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if (empty($user->getPassword())) {
                $user->setPassword($passwordOld);
            }else {
                $plaintextPassword = $user->getPassword();
                $hashedPassword = $passwordHasher->hashPassword(
                    $user,
                    $plaintextPassword
                );
                $user->setPassword($hashedPassword);
            }
            $userRepository->save($user, true);
            $this->addFlash('success', 'User updated successfully.');
            return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
        }
        $html = $this->renderView('backend/user/edit_modal.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
        $datos = array(
            'code' => 200,
            'message' => '',
            'html' => $html,
        );
        return new JsonResponse($datos);

    }


    #[Route('/{id}', name: 'app_user_delete', methods: ['POST'])]
    public function delete(Request $request, User $user, UserRepository $userRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $userRepository->remove($user, true);
        }

        return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
    }
}
