<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Permiso;
class PermisoFixture extends Fixture
{
    public const PERMISO_ROLE_ADMIN = 'admin-user';
    
    public function load(ObjectManager $manager)
    {
        $permisoBack = new Permiso();
        $permisoBack->setRole('ROLE_BACKEND');
        $manager->persist($permisoBack);
        
        $permisoAdmin = new Permiso();
        $permisoAdmin->setRole('ROLE_ADMIN');
        $manager->persist($permisoAdmin);
        
        $manager->flush();
        
        $this->addReference(self::PERMISO_ROLE_ADMIN, $permisoAdmin);
    }
}
