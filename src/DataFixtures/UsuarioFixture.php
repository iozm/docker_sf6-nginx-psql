<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use App\DataFixtures\PermisoFixture;
use App\Entity\User;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;


class UsuarioFixture extends Fixture implements DependentFixtureInterface
{
    private $passwordInterface;
    
     public function __construct(UserPasswordHasherInterface $passwordEncoder)
    {
        $this->passwordInterface = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $usuario = new User();
        $usuario->setEmail('iozavalam@email.com');
        $usuario->setPassword($this->passwordInterface->hashPassword(
            $usuario,
            'xswedcxs'
        ));
        $usuario->setNombre('iozavalam');
        $usuario->setApellidoPaterno('iozavalam');
        $usuario->setApellidoMaterno('iozavalam');
        $usuario->setActivo(true);
        
        $usuario->addPermiso($this->getReference(PermisoFixture::PERMISO_ROLE_ADMIN));
        
        $manager->persist($usuario);
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            PermisoFixture::class,
        );
    }
    


}
