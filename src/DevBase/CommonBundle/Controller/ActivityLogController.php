<?php

namespace App\DevBase\CommonBundle\Controller;

use App\DevBase\CommonBundle\Entity\ActivityLog;
use App\DevBase\CommonBundle\Repository\ActivityLogRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\DevBase\CommonBundle\Service\ActivityLogDTService;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/admin/activity/log")
 */
class ActivityLogController extends AbstractController
{
    /**
     * @Route("/", name="admin_activity_log_index", methods={"GET"})
     */
    public function index(ActivityLogRepository $activityLogRepository): Response
    {
        new ActivityLog();
        $this->denyAccessUnlessGranted('index', $activityLogRepository);
        return $this->render('activity_log/index.html.twig', [
            'activity_logs' => $activityLogRepository->findAll(),
        ]);
    }
    
    /**
     * @Route("/ajax", name="admin_activity_log_index_ajax", methods={"GET"})
     */
    public function indexAjax(Request $request, ActivityLogDTService $activityLogDTService): Response
    {        
        
        $datos = $activityLogDTService->init($request->query->all());
        $respuesta = new JSONResponse($datos);
        return $respuesta;
    }

    /**
     * @Route("/{id}/show", name="admin_activity_log_show", methods={"GET"})
     */
    public function show(ActivityLog $activityLog): Response
    {
        return $this->render('activity_log/show.html.twig', [
            'activity_log' => $activityLog,
        ]);
    }
}
