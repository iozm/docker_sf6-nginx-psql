<?php

namespace App\DevBase\CommonBundle\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * Data transformation class
 *
 * @author Gregwar <g.passault@gmail.com>
 */
class SearchKeyTransformer implements DataTransformerInterface
{
    private $propertyDescription;

    public function __construct($propertyDescription)
    {
        $this->propertyDescription = $propertyDescription;
    }

    public function transform($data)
    {
        if (null === $data) {
            return array('key' => null, 'description' => null);
        }
        
        $propertyAccessor = new PropertyAccessor();
        $description = $propertyAccessor->getValue($data, $this->propertyDescription);
        
        return array('key' => $data, 'description' => $description);
    }

    public function reverseTransform($data)
    {
        return $data['key'];
    }
}
