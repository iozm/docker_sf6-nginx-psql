<?php
/**
 * Created by PhpStorm.
 * User: araujo
 * Date: 1/10/19
 * Time: 12:28 PM
 */

namespace App\DevBase\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\DevBase\CommonBundle\Repository\ActivityLogRepository;


/**
 * @ORM\Entity(repositoryClass=ActivityLogRepository::class)
 * @ORM\Table(name="activity_log")
 */
class ActivityLog
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(name="entity_id", type="integer", length=11, nullable= false)
     */
    protected $entityId;

    /**
     * @ORM\Column(name="entity_class", type="text", length=255, nullable= false)
     */
    protected $entityClass;
    /**
     * @ORM\Column(name="user_id", type="integer", length=11, nullable= false)
     */
    protected $user;

    /**
     * @ORM\Column(name="username", type="text", nullable= false)
     */
    protected $userName;

    /**
     * @ORM\Column(name="activity", type="text", length=8, nullable= false)
     */
    protected $activity;

    /**
     * @ORM\Column(name="ip", type="string", length=16, nullable=false)
     */
    protected $ip;

    /**
     * @var \DateTime
     
     * @ORM\Column(name="date", type="datetime")
     */
    protected $date;

    /**
     * @ORM\Column(name="observations", type="text", nullable= false)
     */
    protected $observations;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEntityId(): ?int
    {
        return $this->entityId;
    }

    public function setEntityId(int $entityId): self
    {
        $this->entityId = $entityId;

        return $this;
    }

    public function getEntityClass(): ?string
    {
        return $this->entityClass;
    }

    public function setEntityClass(string $entityClass): self
    {
        $this->entityClass = $entityClass;

        return $this;
    }

    public function getUser(): ?int
    {
        return $this->user;
    }

    public function setUser(int $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUserName(): ?string
    {
        return $this->userName;
    }

    public function setUserName(string $userName): self
    {
        $this->userName = $userName;

        return $this;
    }

    public function getActivity(): ?string
    {
        return $this->activity;
    }

    public function setActivity(string $activity): self
    {
        $this->activity = $activity;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }
}
