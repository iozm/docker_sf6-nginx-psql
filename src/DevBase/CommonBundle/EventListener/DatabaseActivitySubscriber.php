<?php

// src/EventListener/DatabaseActivitySubscriber.php
namespace App\DevBase\CommonBundle\EventListener;

use App\DevBase\CommonBundle\Entity\ActivityLog;
use Doctrine\Common\EventSubscriber;
//use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Events;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class DatabaseActivitySubscriber implements EventSubscriber
{
    protected   $security;
    protected   $requestStack;
    protected   $em;
    private $idTmp;
    //private     $changes=[];
    private $invalidClasses = array(
        'App\DevBase\CommonBundle\Entity\ActivityLog', 
    );

    public function __construct(Security $security,  RequestStack $requestStack, EntityManagerInterface $em)
    {
        
        // Avoid calling getUser() in the constructor: auth may not
        // be complete yet. Instead, store the entire Security object.
        $this->security = $security;
        $this->requestStack = $requestStack;
        $this->em = $em;
    }

    // this method can only return the event names; you cannot define a
    // custom method name to execute when each event triggers
    public function getSubscribedEvents()
    {
        return [
            Events::postPersist,            
            Events::postRemove,            
            Events::preRemove,
            Events::postUpdate,
        ];
    }

    // callback methods must be called exactly like the events they listen to;
    // they receive an argument of type LifecycleEventArgs, which gives you access
    // to both the entity object of the event and the entity manager itself
    public function postPersist(LifecycleEventArgs $args)
    {
        $this->logActivity('new', $args);
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->logActivity('update', $args);
    }
    
     public function preRemove(LifecycleEventArgs $args)
    {
        $this->idTmp = $args->getEntity()->getId();
        
        return $this;
    }

    public function postRemove(LifecycleEventArgs $args)
    {
        $this->logActivity('remove', $args);
    }

    private function logActivity(string $action, LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        $em = $args->getObjectManager();
        $class = get_class($entity);
        $user = $this->security->getUser();
        if (!in_array( $class, $this->invalidClasses )) {
            $this->addLog($user, $entity, $class, $action);
        }
        
        return $this;
    }

    public function addLog($user, $entity, $class, $action)
    {
        $id = ($action == 'remove') ? $this->idTmp : $entity->getId();
        
        if($user) {
            $log = new ActivityLog();
            $log->setEntityClass($class);
            $log->setEntityId($id);
            $log->setUser( $user->getId() );
            $log->setUserName($user->getUsername());
            $log->setActivity( $action );
            $log->setDate( new \DateTime() );
            $log->setIp($this->requestStack->getCurrentRequest()->getClientIp());
            $log->setObservations('');
           $this->em->persist($log);
           $this->em->flush();
           
        }
        
        return $this;
    }
}