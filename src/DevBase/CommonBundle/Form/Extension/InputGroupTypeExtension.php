<?php

namespace App\DevBase\CommonBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccess;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of InputGroupTypeExtension
 *
 * @author jsr
 */
class InputGroupTypeExtension extends AbstractTypeExtension
{
    /**
     * Return the class of the type being extended.
     */
    public static function getExtendedTypes(): iterable
    {
        // return FormType::class to modify (nearly) every field in the system
        return [TextType::class];
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        // makes it legal for FileType fields to have an image_property option
        $resolver->setDefined(['input_group']);
    }
    
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        
        if (isset($options['input_group'])) {
            
            // this will be whatever class/entity is bound to your form (e.g. Media)
            $parentData = $form->getParent()->getData();
            
            $inputGroup = null;
            if (null !== $parentData) {
                
                //$accessor = PropertyAccess::createPropertyAccessor();
                //$inputGroup = $accessor->getValue($parentData, $options['input_group']);
            }

            // sets an "image_url" variable that will be available when rendering this field
            $view->vars['input_group'] = $options['input_group'];
            
        }
    }
}
