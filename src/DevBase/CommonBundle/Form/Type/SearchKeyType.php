<?php

namespace App\DevBase\CommonBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\DevBase\CommonBundle\Form\Type\EntityIdType;
use App\DevBase\CommonBundle\DataTransformer\SearchKeyTransformer;

/**
 * Entity identitifer
 *
 * @author Gregwar <g.passault@gmail.com>
 */
class SearchKeyType extends AbstractType
{
    

    public function __construct()
    {
        
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('key', EntityIdType::class, array(
            'class' => $options['class'],
            'property' => $options['property'],
            'hidden' => $options['key_hidden'],
            'attr' => array('readonly' => $options['key_readonly'])
        ))
        ->add('description', TextType::class, array(
            'attr' => array('readonly' => $options['description_readonly'])
        ));
        
        $builder->addModelTransformer(new SearchKeyTransformer($options['property_description']));
        /*
        $builder->addModelTransformer(new EntityToIdTransformer(
            $this->registry->getManager($options['em']),
            
            $options['query_builder'],
            $options['multiple']
        ));
         * 
         */
    }

    // Todo: remove when Symfony < 2.7 support is dropped
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(array(
            'class', 
            'property_description'
        ));

        $resolver->setDefaults(array(
            'em'            => null,
            'property'      => null,
            'query_builder' => null,
            'key_hidden'        => true,
            'multiple'      => false,
            'input_group'   => 'fas fa-search',
            'key_readonly'  => true,
            'description_readonly' => true,
            
            
        ));
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['key_hidden'] = $options['key_hidden'];
        $view->vars['input_group'] = $options['input_group'];
        
    }
    

    public function getBlockPrefix()
    {
        return 'search_id';
    }

    // BC for SF < 2.8
    public function getName()
    {
        return $this->getBlockPrefix();
    }
}
