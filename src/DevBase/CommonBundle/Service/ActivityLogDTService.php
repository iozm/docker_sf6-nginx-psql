<?php

namespace App\DevBase\CommonBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use App\DevBase\CommonBundle\Service\BaseDT;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TramiteDT
 *
 * @author jsr
 */
class ActivityLogDTService
{
    private $em;
    private $baseDT;
    private $router;
    private $security;
    
    public function __construct(EntityManagerInterface $em, RouterInterface $router, BaseDT $baseDT, Security $security)
    {
        $this->em = $em;
        $this->baseDT = $baseDT;
        $this->router = $router;
        $this->baseDT->init($em);
        $this->security = $security;
    }
    
    public function init($request)
    {
        
        $columns = array(
            array( 'db' => 'entity_id', 'dt' => 0 ),
            array( 'db' => 'entity_class',  'dt' => 1 ),
            array( 'db' => 'user',  'dt' => 2 ),
            array( 'db' => 'username',   'dt' => 3 ),
            array( 'db' => 'activity',   'dt' => 4 ),
            array( 'db' => 'ip',   'dt' => 5 ),
            array( 'db' => 'date',   'dt' => 6 ),
        );
        $connection = $this->em->getConnection();
        $data = $this->simple($request, $connection, 'common.activity_log', 'id', $columns);
        
        return $data;
    }
    
    /**
        * Perform the SQL queries needed for an server-side processing requested,
        * utilising the helper functions of this class, limit(), order() and
        * filter() among others. The returned array is ready to be encoded as JSON
        * in response to an SSP request, or can be modified if needed before
        * sending back to the client.
        *
        *  @param  array $request Data sent to server by DataTables
        *  @param  array|PDO $conn PDO connection resource or connection parameters array
        *  @param  string $table SQL table to query
        *  @param  string $primaryKey Primary key of the table
        *  @param  array $columns Column information array
        *  @return array          Server-side processing response array
    */
    public function simple ( $request, $conn, $table, $primaryKey, $columns )
    {
        
            $bindings = array();
            

            // Build the SQL query string from the request
            $limit = $this->baseDT->limit( $request, $columns );
            
            $order = $this->baseDT->order( $request, $columns );
            
            $where = $this->baseDT->filter( $request, $columns, $bindings );
            
            $where = $this->agregarFiltrosExtra($where);
            
            
            
            // Main query to actually get the data
            $data = $this->baseDT->sql_exec( $conn, $bindings,
                    "SELECT  id ,  ".implode(" ,  ", $this->baseDT->pluck($columns, 'db'))." 
                     FROM  $table   
                     $where
                     $order
                     $limit"
            );
            
            // Data set length after filtering
            $resFilterLength = $this->baseDT->sql_exec( $conn, $bindings,
                    "SELECT COUNT( {$primaryKey} )
                     FROM    $table 
                     $where"
            );
            
            
            $recordsFiltered = $resFilterLength[0][0];
            
            

            // Total data set length
            $resTotalLength = $this->baseDT->sql_exec( $conn,
                    "SELECT COUNT( {$primaryKey} )
                     FROM    $table "
            );
            $recordsTotal = $resTotalLength[0][0];

            /*
             * Output
             */
            return array(
                    "draw"            => isset ( $request['draw'] ) ?
                            intval( $request['draw'] ) :
                            0,
                    "recordsTotal"    => intval( $recordsTotal ),
                    "recordsFiltered" => intval( $recordsFiltered ),
                    "data"            => $this->data_output( $columns, $data )
            );
    }
    
    public function agregarFiltrosExtra($where)
    {
        if ( $where === '' ) {
                $where = 'WHERE ';
        }
        
        $filtrosExtra = '';
        if($this->security->isGranted(array('ROLE_ADMIN'))) {
            
        }  else {
            $filtrosExtra = ' id < 0';
        }
        
        if ( $where === 'WHERE ' ) {
                $where = '';
        }
        
        return $where.$filtrosExtra;
    }
    
    /**
    * Create the data output array for the DataTables rows
    *
    *  @param  array $columns Column information array
    *  @param  array $data    Data from the SQL get
    *  @return array          Formatted data in a row based format
    */
    public function data_output ( $columns,  $data )
    {
        $out = array();

        for ( $i=0, $ien=count($data) ; $i<$ien ; $i++ ) {
               $row = array();

               for ( $j=0, $jen=count($columns) ; $j<$jen ; $j++ ) {
                       $column = $columns[$j];

                       // Is there a formatter?
                       if ( isset( $column['formatter'] ) ) {
                               $row[ $column['dt'] ] = $column['formatter']( $data[$i][ $column['db'] ], $data[$i] );
                       }
                       else {
                               $row[ $column['dt'] ] = $data[$i][ $columns[$j]['db'] ];
                       }
               }

               $url = $this->router->generate('admin_activity_log_show', array(
                   'id' => $data[$i]['id'],
                   ));
               
               $btnEdit = '<a href="'.$url.'" class="btn btn-primary btn-sm m-btn m-btn--icon m-btn--icon-only" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="Consultar"><i class="fas fa-search"></i> </a>';
               

               $row[] = $btnEdit;
               $out[] = $row;
        }

        return $out;
    }
}
