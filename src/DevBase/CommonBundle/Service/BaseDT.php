<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\DevBase\CommonBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use PDO;

/**
 * Description of BaseDT
 *
 * @author jsr
 */
class BaseDT {
    /**
     *
     * @var Doctrine\ORM\EntityManagerInterface
     */
    private  $em;

    public function init(EntityManagerInterface $em): self {
        $this->em = $em;
        return $this;
    }

    /**
    * Create the data output array for the DataTables rows
    *
    *  @param  array $columns Column information array
    *  @param  array $data    Data from the SQL get
    *  @return array          Formatted data in a row based format
    */
    public function data_output ( $columns,  $data )
    {
        $out = array();

        for ( $i=0, $ien=count($data) ; $i<$ien ; $i++ ) {
               $row = array();

               for ( $j=0, $jen=count($columns) ; $j<$jen ; $j++ ) {
                       $column = $columns[$j];

                       // Is there a formatter?
                       if ( isset( $column['formatter'] ) ) {
                               $row[ $column['dt'] ] = $column['formatter']( $data[$i][ $column['db'] ], $data[$i] );
                       }
                       else {
                               $row[ $column['dt'] ] = $data[$i][ $columns[$j]['db'] ];
                       }
               }
               $row[] = 'OActions';
               $out[] = $row;
        }

        return $out;
    }
    
    /**
	 * Pull a particular property from each assoc. array in a numeric array, 
	 * returning and array of the property values from each item.
	 *
	 *  @param  array  $a    Array to get data from
	 *  @param  string $prop Property to read
	 *  @return array        Array of property values
	 */
    public function pluck ( $a, $prop )
    {
            $out = array();

            for ( $i=0, $len=count($a) ; $i<$len ; $i++ ) {
                    $out[] = $a[$i][$prop];
            }

            return $out;
    }
    
    /**
	 * Ordering
	 *
	 * Construct the ORDER BY clause for server-side processing SQL query
	 *
	 *  @param  array $request Data sent to server by DataTables
	 *  @param  array $columns Column information array
	 *  @return string SQL order by clause
	 */
    public function order ( $request, $columns )
    {
        $order = '';

        if ( isset($request['order']) && count($request['order']) ) {
                $orderBy = array();
                $dtColumns = $this->pluck( $columns, 'dt' );

                for ( $i=0, $ien=count($request['order']) ; $i<$ien ; $i++ ) {
                        // Convert the column index into the column data property
                        $columnIdx = intval($request['order'][$i]['column']);
                        $requestColumn = $request['columns'][$columnIdx];

                        $columnIdx = array_search( $requestColumn['data'], $dtColumns );
                        $column = $columns[ $columnIdx ];

                        if ( $requestColumn['orderable'] == 'true' ) {
                                $dir = $request['order'][$i]['dir'] === 'asc' ?
                                        'ASC' :
                                        'DESC';

                                $orderBy[] = ' '.$column['db'].'  '.$dir;
                        }
                }

                if ( count( $orderBy ) ) {
                        $order = 'ORDER BY '.implode(', ', $orderBy);
                }
        }

        return $order;
    }
    
    /**
        * Paging
        *
        * Construct the LIMIT clause for server-side processing SQL query
        *
        *  @param  array $request Data sent to server by DataTables
        *  @param  array $columns Column information array
        *  @return string SQL limit clause
    */
    public function limit ( $request, $columns )
    {
        $limit = '';

        if ( isset($request['start']) && $request['length'] != -1 ) {
                $limit = "LIMIT ".intval($request['length'])." OFFSET ".intval($request['start']);
        }

        return $limit;
    }
    
    /**
	 * Create a PDO binding key which can be used for escaping variables safely
	 * when executing a query with sql_exec()
	 *
	 * @param  array &$a    Array of bindings
	 * @param  *      $val  Value to bind
	 * @param  int    $type PDO field type
	 * @return string       Bound key to be used in the SQL where this parameter
	 *   would be used.
	 */
    public function bind ( &$a, $val, $type )
    {
        $key = ':binding_'.count( $a );

        $a[] = array(
                'key' => $key,
                'val' => $val,
                'type' => $type
        );

        return $key;
    }
    
    /**
	 * Searching / Filtering
	 *
	 * Construct the WHERE clause for server-side processing SQL query.
	 *
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here performance on large
	 * databases would be very poor
	 *
	 *  @param  array $request Data sent to server by DataTables
	 *  @param  array $columns Column information array
	 *  @param  array $bindings Array of values for PDO bindings, used in the
	 *    sql_exec() function
	 *  @return string SQL where clause
	 */
    public function filter ( $request, $columns, &$bindings )
    {
        $globalSearch = array();
        $columnSearch = array();
        $dtColumns = $this->pluck( $columns, 'dt' );

        if ( isset($request['search']) && $request['search']['value'] != '' ) {
                $str = $request['search']['value'];

                for ( $i=0, $ien=count($request['columns']) ; $i<$ien ; $i++ ) {
                        $requestColumn = $request['columns'][$i];
                        $columnIdx = array_search( $requestColumn['data'], $dtColumns );
                        $column = $columns[ $columnIdx ];

                        if ( $requestColumn['searchable'] == 'true' ) {
                                $binding = $this->bind( $bindings, '%'.$str.'%', \PDO::PARAM_STR );
                                $globalSearch[] = " ".$column['db']."  LIKE ".$binding;
                        }
                }
        }

        // Individual column filtering
        if ( isset( $request['columns'] ) ) {
                for ( $i=0, $ien=count($request['columns']) ; $i<$ien ; $i++ ) {
                        $requestColumn = $request['columns'][$i];
                        $columnIdx = array_search( $requestColumn['data'], $dtColumns );
                        $column = $columns[ $columnIdx ];

                        $str = $requestColumn['search']['value'];

                        if ( $requestColumn['searchable'] == 'true' &&
                         $str != '' ) {
                                $binding = $this->bind( $bindings, '%'.$str.'%', \PDO::PARAM_STR );
                                $columnSearch[] = " ".$column['db']."  LIKE ".$binding;
                        }
                }
        }

        // Combine the filters into a single string
        $where = '';

        if ( count( $globalSearch ) ) {
                $where = '('.implode(' OR ', $globalSearch).')';
        }

        if ( count( $columnSearch ) ) {
                $where = $where === '' ?
                        implode(' AND ', $columnSearch) :
                        $where .' AND '. implode(' AND ', $columnSearch);
        }

        if ( $where !== '' ) {
                $where = 'WHERE '.$where;
        }

        return $where;
    }
    
    /**
        * Execute an SQL query on the database
        *
        * @param  resource $db  Database handler
        * @param  array    $bindings Array of PDO binding values from bind() to be
        *   used for safely escaping strings. Note that this can be given as the
        *   SQL query string if no bindings are required.
        * @param  string   $sql SQL query to execute.
        * @return array         Result from the query (all rows)
        */
    public function sql_exec ( $conn, $bindings, $sql=null )
    {
            //$conn = $this->em->getConnection();
            // Argument shifting
            if ( $sql === null ) {
                    $sql = $bindings;
            }
            
            //$stmt = $db->prepare( $sql );
            $stmt = $conn->prepare($sql);
            //echo $sql;

            // Bind parameters
            if ( is_array( $bindings ) ) {
                    for ( $i=0, $ien=count($bindings) ; $i<$ien ; $i++ ) {
                            $binding = $bindings[$i];
                            $stmt->bindValue( $binding['key'], $binding['val'], $binding['type'] );
                    }
            }

            // Execute
            try {
                    $stmt->execute();
            }
            catch (PDOException $e) {
                    throw new LogicException($e->getMessage());
            }

            // Return all
            //return $stmt->fetchAll( PDO::FETCH_BOTH );
            return $stmt->fetchAll( PDO::FETCH_BOTH );
    }
    
    /**
        * Perform the SQL queries needed for an server-side processing requested,
        * utilising the helper functions of this class, limit(), order() and
        * filter() among others. The returned array is ready to be encoded as JSON
        * in response to an SSP request, or can be modified if needed before
        * sending back to the client.
        *
        *  @param  array $request Data sent to server by DataTables
        *  @param  array|PDO $conn PDO connection resource or connection parameters array
        *  @param  string $table SQL table to query
        *  @param  string $primaryKey Primary key of the table
        *  @param  array $columns Column information array
        *  @return array          Server-side processing response array
    */
    public function simple ( $request, $conn, $table, $primaryKey, $columns )
    {
        
            $bindings = array();
            

            // Build the SQL query string from the request
            $limit = $this->limit( $request, $columns );
            
            $order = $this->order( $request, $columns );
            
            $where = $this->filter( $request, $columns, $bindings );

            // Main query to actually get the data
            $data = $this->sql_exec( $conn, $bindings,
                    "SELECT  ".implode(" ,  ", $this->pluck($columns, 'db'))." 
                     FROM  $table 
                     $where
                     $order
                     $limit"
            );

            
            // Data set length after filtering
            $resFilterLength = $this->sql_exec( $conn, $bindings,
                    "SELECT COUNT( {$primaryKey} )
                     FROM    $table 
                     $where"
            );
            
            
            $recordsFiltered = $resFilterLength[0][0];
            
            

            // Total data set length
            $resTotalLength = $this->sql_exec( $conn,
                    "SELECT COUNT( {$primaryKey} )
                     FROM    $table "
            );
            $recordsTotal = $resTotalLength[0][0];

            /*
             * Output
             */
            return array(
                    "draw"            => isset ( $request['draw'] ) ?
                            intval( $request['draw'] ) :
                            0,
                    "recordsTotal"    => intval( $recordsTotal ),
                    "recordsFiltered" => intval( $recordsFiltered ),
                    "data"            => $this->data_output( $columns, $data )
            );
    }
    
    //put your code here
}
