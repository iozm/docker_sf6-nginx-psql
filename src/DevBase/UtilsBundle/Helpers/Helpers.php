<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\DevBase\UtilsBundle\Helpers;

/**
 * Description of Helpers
 *
 * @author jsr
 */
class Helpers 
{
    public static function getSubString($string, $length=NULL)
    {
        //Si no se especifica la longitud por defecto es 50
        if ($length == NULL)
            $length = 100;
        //Primero eliminamos las etiquetas html y luego cortamos el string
        $stringDisplay = substr(strip_tags($string), 0, $length);
        //Si el texto es mayor que la longitud se agrega puntos suspensivos
        if (strlen(strip_tags($string)) > $length)
            $stringDisplay .= ' ...';
        return $stringDisplay;
    }
    
    static public function slugify($text)
    {
        // replace all non letters or digits by -
        $text = preg_replace('/\W+/', '_', $text);

        // trim and lowercase
        $text = strtolower(trim($text, '-'));

        return $text;
    }
    
    static public function generateUniqueValue($length = 10) 
    {
        $key = md5(uniqid(rand(), true));
        /** Pendiente dejar la longitud como parámetros */
        return substr($key, 0, $length); 
    }
    
}
