<?php

namespace App\PDF;
use App\DevBase\UtilsBundle\Helpers\DateToText;

class DocumentoPDF
{
    public function __construct()
    {
        //$this->dateToText = new DateToText();
    }

    public function generar(\TCPDF $pdf, array $datos)
    {

       // $pdf->AddPage();
        $pdf->setY(35);
        $pdf->setFont('helvetica', 'B', '15');
        $tituloDocumento = 'COMPROBANTE DE VENTA';
        $pdf->writeHTMLCell(0, 0, '', '', $tituloDocumento, 0, 1, 0, true, 'C', true);

        $pdf->setY(50);
        $pdf->setFont('helvetica', '', '10');
        $fecha = new DateToText($datos['fecha']);
        $pdf->MultiCell(0,0, 'CHILPANCINGO, GRO., A'.' '.$fecha->getText().'.', 0, 'R', 0, 0, '', '', true, 0, true, true, 5, 'M');

        $pdf->setY(70);
        $pdf->MultiCell(0, 0, "<b>DATOS DEL CLIENTE</b>", 0, 'L', 0, 1, '', '', true, 0, true);
        $pdf->MultiCell(0,0, "<b>NOMBRE</b>: " .$datos['cliente'], 0, 'L', 0, 0, '', '', true, 0, true, true, 5, 'M');
        $pdf->Ln(4);
        $pdf->MultiCell(0,0, "<b>RFC</b>: " .$datos['rfcCliente'], 0, 'L', 0, 0, '', '', true, 0, true, true, 5, 'M');
        $pdf->Ln(4);
        $pdf->MultiCell(0,0, "<b>TELEFONO</b>: " .$datos['telefonoCliente'], 0, 'L', 0, 0, '', '', true, 0, true, true, 5, 'M');
        $pdf->Ln(4);
        $pdf->MultiCell(0,0, "<b>EMAIL</b>: " .$datos['emailCliente'], 0, 'L', 0, 0, '', '', true, 0, true, true, 5, 'M');
        $pdf->Ln(15);
        $pdf->MultiCell(0, 0, "<b>DESGLOSE DE ARTICULOS</b>", 0, 'C', 0, 1, '', '', true, 0, true);
        $pdf->Ln(4);

        /*if (count($datos['articulos']) > 0) {
            foreach ($datos['articulos'] as $articulo) {
                $nombreArticulo = $articulo->getArticulo()->getNombre();
            $tbl =  '<table cellspacing="0" cellpadding="1" border="1">
                        <thead>
                            <tr>
                                <th rowspan="1" ><h4 align="center">ARTICULO</h4></th>
                                <th rowspan="1"><h4 align="center">CANTIDAD</h4></th>
                                <th rowspan="1"><h4 align="center">PRECIO</h4></th>
                                <th rowspan="1"><h4 align="center">DESCUENTO</h4></th>
                            </tr>
                        </thead>';
            $tbl .= '<tbody>';
                foreach ($datos['articulos'] as $articulo) {
                    $tbl .= '<tr>';
                    $tbl .= '<td>' . $articulo->getArticulo()->getNombre() . '</td>';
                    $tbl .= '<td>' . $articulo->getCantidad() . '</td>';
                    $tbl .= '<td>' . $articulo->getPrecioVenta() . '</td>';
                    $tbl .= '<td>' . $articulo->getDescuento() . '</td>';
                    $tbl .= '</tr>';
                }
                $tbl .= '</tbody>';
            $tbl .= '</table>';
            $pdf->MultiCell(0,0, $tbl, 0, 'L', 0, 0, '', '', true, 0, true, true, 5, 'M');
        } else {
            $pdf->MultiCell(0,0, "<b>SIN ARTICULOS</b>", 0, 'C', 0, 0, '', '', true, 0, true, true, 5, 'M');
        } */

        $pdf->Ln(5);
        $pdf->SetFont('helvetica', 'B', '10');

        if (count($datos['articulos']) > 0) {
            $pdf->MultiCell(100, 5, 'ARTICULO', 'LTRB', 'C', 0, 0);
            $pdf->MultiCell(30, 5, 'CANTIDAD', 'LTRB', 'C', 0, 0);
            $pdf->MultiCell(30, 5, 'PRECIO', 'LTRB', 'C', 0, 0);
            $pdf->MultiCell(30, 5, 'DESCUENTO', 'LTRB', 'C', 0, 1);
            $pdf->SetFont('helvetica', '', '10');
                foreach ($datos['articulos'] as $articulo) {
                    $pdf->MultiCell(100, 10, $articulo->getArticulo()->getNombre(), 'LTRB', 'L', 0, 0);
                    $pdf->MultiCell(30, 10, $articulo->getCantidad() . " PZAS", 'LTRB', 'C', 0, 0);
                    $pdf->MultiCell(30, 10, "$ " . $articulo->getPrecioVenta(), 'LTRB', 'C', 0, 0);
                    $pdf->MultiCell(30, 10, "$ " . $articulo->getDescuento(), 'LTRB', 'C', 0, 1);
                }
        } else {
            $pdf->SetFont('helvetica', 'B', '20');
            $pdf->MultiCell(0,0, "<b>SIN ARTICULOS</b>", 0, 'C', 0, 0, '', '', true, 0, true, true, 5, 'M');
        }

        $pdf->Ln(5);
        $pdf->SetFont('helvetica', '', '10');
        $pdf->MultiCell(0,0, "<b>TOTAL</b>: " .$datos['total'], 0, 'R', 0, 0, '', '', true, 0, true, true, 5, 'M');



        }

}
