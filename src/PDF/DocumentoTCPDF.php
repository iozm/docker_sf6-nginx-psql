<?php

namespace App\PDF;

use App\PDF\MyTCPDF;

class DocumentoTCPDF extends MyTCPDF
{
    private $projectDir;
    public function Header()
    {

        // get the current page break margin
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);

        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();

        if ($this->header_xobjid === false) {

            $this->header_xobjid = $this->startTemplate($this->w, $this->tMargin);

            $headerfont = $this->getHeaderFont();

            $headerdata = $this->getHeaderData();

            $this->y = $this->header_margin;

            //Colocando imagen secundaria
            if ($this->rtl) {
                $this->x = $this->w - $this->original_rMargin;
            } else {
                $this->x = 11;
            }

            //Imprimiendo logo
            //dd($this->GetY());
            $this->Image($this->projectDir.'/public/imags/CacTics.jpg', '', 0, 45);
            //$this->x = $this->x + 40;

            $imgly = $this->getImageRBY();
            $cell_height = $this->getCellHeight($headerfont[2] / $this->k);
            if ($this->getRTL()) {
                $header_x = $this->original_rMargin + ($headerdata['logo_width'] * 1.1);
            } else{
                $header_x = $this->original_lMargin + ($headerdata['logo_width'] * 1.1);
            }

            $this->setX($header_x);
            $cw = $this->w - $this->original_lMargin - $this->original_rMargin - (40 * 1.1) - (100 * 1.1 - 30);
            //dd($headerfont[0]);
            $this->SetFont($headerfont[0], 'B', 10);
            $this->setColor('text',0, 0, 0);
            $this->setX(0);
            $headerText = 'ADVentas S.A. de C.V.';
            $headerTextRfc = 'RFC: 123456789012';
            $headerTextDireccion = 'DIRECCIÓN: Calle 1 # 2-3';
            $headerTextTelefono = 'TEL. 1234567890';
            $this->Cell(0,15, $headerText, 0, false, 'C', 0,'',0, false, 'M', 'M');
            $this->Ln(4);
            $this->Cell(0,15, $headerTextRfc, 0, false, 'C', 0,'',0, false, 'M', 'M');
            $this->Ln(4);
            $this->Cell(0,15, $headerTextDireccion, 0, false, 'C', 0,'',0, false, 'M', 'M');
            $this->Ln(4);
            $this->Cell(0,15, $headerTextTelefono, 0, false, 'C', 0,'',0, false, 'M', 'M');
            //$this->MultiCell(0, $cell_height, $headerText, '', 'J', 0, 1, '','',true,0,true);
            $this->SetFont($headerfont[0], 'B', 50);
            $this->SetX($header_x);
            $this->MultiCell($cw, $cell_height, "", 0, 'C', 0, 1);

            $this->SetLineStyle(array('width' => 0.85 / $this->k, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => $headerdata['line_color']));

            $this->SetY((2.835 / $this->k) + max($imgly, $this->y));
            //Imprimiendo la linea de final de encabezado
            $this->SetTextColorArray(array(88,88,90));
            if ($this->rtl) {
                $this->x = $this->w - $this->original_rMargin;
            } else {
                $this->x = $this->original_lMargin;
            }
            //$this->Cell(($this->w - $this->original_lMargin - $this->original_rMargin), 0, '', 'B', 0, 'R');
            $this->endTemplate();
        }
        // print header template
        $x = 0;
        $dx = 0;
        if (!$this->header_xobj_autoreset AND $this->booklet AND (($this->page % 2) == 0)) {
            // adjust margins for booklet mode
            $dx = ($this->original_lMargin - $this->original_rMargin);

        }
        if ($this->rtl) {
            $x = $this->w + $dx;

        } else {

            $x = 0 + $dx;
        }

        $this->printTemplate($this->header_xobjid, $x, 0, 0, 0, '', '', false);
        if ($this->header_xobj_autoreset) {
            // reset header xobject template at each page
            $this->header_xobjid = false;
        }
    }

    public function Footer() {
        $this->setColor('text', 0, 0, 0);
        $txtFooter = 'SUPER PERSONAL S.A. DE C.V. TIENDITA LA CERVECITA EL AMIGO PETER';
        $this->SetFont('helvetica', '', 7);
        $this->MultiCell(0,0,$txtFooter,0,'L',0,1,'','',true,0,true);
        $margins = $this->getMargins();
        $wPage = $this->getPageWidth() - $margins['left'] - $margins['right'];
       // $this->Image($this->projectDir."/public/images/pleca_2021_0.png", '', '', $wPage);
    }

    public function init($string = null, $footerText = null, string $projectDir)
    {
        $this->projectDir = $projectDir;

        if($footerText){
            $this->setFooterText($footerText);
        }
        $headerTitle = "Teste";
        $this->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $headerTitle,
            mb_strtoupper($string, 'UTF-8')
        );
        //dd($headerTitle);
        $this->setFooterData(array(0,64,0), array(0,64,128));
        // set header and footer fonts
        $this->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $this->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        //dd($this->getMargins());

        $this->SetMargins(10, 40,10);


        $this->SetHeaderMargin(10);
        $this->SetFooterMargin(20);
        //dd($this->getMargins());
        // set auto page breaks
        $this->SetAutoPageBreak(TRUE, 20);

        $this->AddPage();
    }
}