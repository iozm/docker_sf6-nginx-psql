<?php

namespace App\PDF;

use \TCPDF;

class MyTCPDF extends \TCPDF
{
    private $footerText = array();

    public function setFooterText(array $txt)
    {
        $this->footerText = $txt;
    }

    public function Header()
    {

    }

    public function Footer()
    {

    }

    public function getFullPageWidth(){
        $margins = $this->getMargins();

        return $this->getPageWidth() - $margins['left'] - $margins['right'];
    }

}